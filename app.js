// Dependecies
const express = require('express')
const app = express()
const config = require('./config')
const parser = require('body-parser')
const path = require('path')
const url = require('url')
const shortUrls = []

let counter = 1

// middleware Initialization
app.use(parser.json())

// Developer Options 
if (process.env.NODE_ENV !== 'production') {
  const morgan = require('morgan')
  app.use(morgan('dev'))
}

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '/public/index.html'))
})

app.get('/new/http:\/\/www.[a-zA-Z0-9]+.[a-zA-Z]{3}', (req, res) => {
  const parseUrl = url.parse(req.url).pathname
  const cuttedUrl = parseUrl.slice(5, parseUrl.length)
  console.log(cuttedUrl)
  const newUrl = { originalUrl: cuttedUrl, newUrl: counter }
  counter++
  shortUrls.push(newUrl)
  res.json(newUrl)
})

app.get('/new/:id', (req, res) => {
  res.send('The url provided does not meet the requirements')
})

app.get('/:id', (req, res) => {
  const seekItem = req.params.id
  for (let i = 0; i < shortUrls.length; i++) {
    if (shortUrls[i].newUrl.toString() === seekItem) {
      return res.redirect(shortUrls[i].originalUrl)
    }
  }
  res.send('There is no such URL in database')
})

app.listen(config.port, () => {
  console.log('currently listening on port: %d', config.port)
})

app.use((req, res) => {
  res.status(404).send('Route not found')
})
